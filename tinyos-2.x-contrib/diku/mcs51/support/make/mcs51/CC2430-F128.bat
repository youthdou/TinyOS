@echo off
if exist C:\Keil\C51\ (
	set KEIL_DIR=C:\Keil\C51
) else if exist D:\Keil\C51\ (
	set KEIL_DIR=D:\Keil\C51
) else (
	echo "Do not find Keil."
)
SET C51INC=%KEIL_DIR%\INC\Chipcon\;%KEIL_DIR%\INC\
SET C51LIB=%KEIL_DIR%\LIB
SET CPU_TYPE=CC2430-F128
SET CPU_VENDOR=Chipcon
SET UV2_TARGET=Target 1
SET CPU_XTAL=0x016E3600

rem %KEIL_DIR%\BIN\C51.EXE "app.c" LARGE BROWSE DEBUG OBJECTEXTEND PRINT(.\app.lst) REGFILE(app.reg) OBJECT(.\app.obj)
rem %KEIL_DIR%\BIN\C51.EXE "app.c" LARGE PRINT(.\app.lst) OPTIMIZE(0,SIZE) OBJECT(.\app.obj)

rem Compile app.c to app.obj
%KEIL_DIR%\BIN\C51.EXE "app.c" LARGE PRINT(.\app.lst) OBJECT(.\app.obj) SYMBOLS

rem Link startup.obj/app.obj and convert to hex
%KEIL_DIR%\BIN\BL51.EXE "startup.obj", "app.obj" TO "app" PRINT (app.map) XDATA( 0X0-0X1EFF ) RAMSIZE(256) REGFILE(app.reg)
%KEIL_DIR%\BIN\OH51.EXE "app" 

rem Link using LX51 which should contain some optimisations...
rem LX51 is only available in the "professional edition"
rem %KEIL_DIR%\BIN\LX51.EXE "startup.obj", "app.obj" TO "app" PRINT (app.map) CLASSES( XDATA(X:0xE000-X:0xFF00), IDATA(I:0-I:0xFF)) REGFILE(app.reg)
rem %KEIL_DIR%\BIN\OHX51.EXE "app" 
@echo on
