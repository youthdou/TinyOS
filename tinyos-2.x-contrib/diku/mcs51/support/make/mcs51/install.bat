@echo off
echo "install app.hex to cc2530"
if exist "C:\Program Files\Texas Instruments\SmartRF Tools\Flash Programmer\bin\SmartRFProgConsole.exe" (
	set TI_TOOL="C:\Program Files\Texas Instruments\SmartRF Tools\Flash Programmer\bin\SmartRFProgConsole.exe"
) else if exist "D:\Program Files (x86)\Texas Instruments\SmartRF Tools\Flash Programmer\bin\SmartRFProgConsole.exe" (
	set TI_TOOL="D:\Program Files (x86)\Texas Instruments\SmartRF Tools\Flash Programmer\bin\SmartRFProgConsole.exe"
) else (
	echo "Do not find Keil."
)
rem SmartRFProgConsole.exe S() EPV F="app.hex" KI(F=0)
%TI_TOOL% S() EP F="app.hex" KI(F=0)
rem pause
@echo on