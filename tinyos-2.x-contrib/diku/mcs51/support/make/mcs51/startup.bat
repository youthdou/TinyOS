echo off
if exist C:\Keil\C51\ (
	set KEIL_DIR=C:\Keil\C51
) else if exist D:\Keil\C51\ (
	set KEIL_DIR=D:\Keil\C51
) else (
	echo "Do not find Keil."
)
echo %KEIL_DIR%
SET C51INC=%KEIL_DIR%\INC\Chipcon\;%KEIL_DIR%\INC\
SET C51LIB=%KEIL_DIR%\LIB
echo on
%KEIL_DIR%\BIN\A51.EXE "startup.a51" SET (SMALL) DEBUG EP
