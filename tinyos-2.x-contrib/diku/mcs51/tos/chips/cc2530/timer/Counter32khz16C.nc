//#include "Timer.h"

configuration Counter32khz16C
{
  provides interface Counter<T32khz,uint16_t> as Counter;
}
implementation {
  components HplCC2530Timer1AlarmCounterC;

  Counter = HplCC2530Timer1AlarmCounterC.Counter;
}
