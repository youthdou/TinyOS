
/*
 * Copyright (c) 2007 University of Copenhagen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 * - Neither the name of University of Copenhagen nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY
 * OF COPENHAGEN OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/**
 * @author Martin Leopold
 */


// Our Timer.h must take precense over tos/lib/timer/Timer.h - the empty structures
// does not work with Keil
#include <Timer.h>
#include <CC2530Timer.h>
#include <ioCC2530.h>
#include "platform.h"

module PlatformP { 
	provides interface Init;
	uses interface Init as LedsInit;
	uses interface Init as GIOInit;
}
implementation {

	void clockSetMainSrc(uint8_t source)
	{
		uint8_t osc32k_bm = CLKCONCMD & CLKCON_OSC32K_BM;

		// Source can have the following values:
		// CLOCK_SRC_XOSC   0x00  High speed Crystal Oscillator (XOSC)
		// CLOCK_SRC_HFRC   0x01  Low power RC Oscillator (HFRC)
		
		SLEEPCMD &= ~SLEEP_OSC_PD_BM;       // power up both oscillators
		while (!CC2530_IS_HFRC_STABLE() || ((SLEEPSTA & SLEEP_OSC_PD_BM)!=0));// wait until the oscillator is stable
		NOP();

		if (source == CLOCK_SRC_HFRC){
			CLKCONCMD = (osc32k_bm | CLKCON_OSC_BM | TICKSPD_DIV_2 | CLKCON_CLKSPD_BM);
		}
		else if (source == CLOCK_SRC_XOSC){
			CLKCONCMD = (osc32k_bm | TICKSPD_DIV_1);
		}
		CC2530_WAIT_CLK_UPDATE();
		SLEEPCMD |= SLEEP_OSC_PD_BM;        // power down the unused oscillator
	}

	uint8_t clockSelect32k(uint8_t source)
	{
		// System clock source must be high frequency RC oscillator before
		// changing 32K source. 
		if( !(CLKCONSTA & CLKCON_OSC_BM) )
		  return FAIL;
		
		if (source == CLOCK_32K_XTAL){
			CLKCONCMD &= ~CLKCON_OSC32K_BM;
		}
		else if (source == CLOCK_32K_RCOSC){
			CLKCONCMD |= CLKCON_OSC32K_BM;
		}
		CC2530_WAIT_CLK_UPDATE();
		
		return SUCCESS;
	}
	
	void halMcuInit()
	{
		// if 32k clock change fails, set system clock to HF RC and try again
		if(clockSelect32k(CLOCK_32K_XTAL) != SUCCESS) {
				clockSetMainSrc(CLOCK_SRC_HFRC);
				if(clockSelect32k(CLOCK_32K_XTAL) != SUCCESS) {
					while(1);
			}
		}
		clockSetMainSrc(CLOCK_SRC_XOSC);
	}

	command error_t Init.init() {

		halMcuInit();
		
		MAKE_IO_PIN_OUTPUT(P1_DIR, 2);
		MAKE_IO_PIN_OUTPUT(P1_DIR, 3);
		P1_2 = 0;
		P1_3 = 0;

		call LedsInit.init();
		return SUCCESS;
	}

  default command error_t LedsInit.init() { return SUCCESS; }
}
